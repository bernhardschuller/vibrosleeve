  // getJSON('/pictures.json', function(data) { pictures = data; });
  var images = [
  {
    "file": "images/monsters-01.png",
    "pattern": "200:1"
  },
  {
    "file": "images/monsters-02.png",
    "pattern": "200:2"
  },
  {
    "file": "images/monsters-03.png",
    "pattern": "200:3"
  },
  {
    "file": "images/monsters-04.png",
    "pattern": "200:4"
  },
  {
    "file": "images/monsters-05.png",
    "pattern": "200:5"
  }
  ];

/*
// get images, place them in an array & randomize the order
for (var i = 0; i < 8; i++) { 
  var rand = Math.floor(Math.random() * (1200 - 900 + 1) + 900); 
  var img = 'http://lolcat.com/images/lolcats/' + rand + '.jpg';
  images.push(img);
  images.push(img);
}
*/
for (var i = 0; i < 5; i++) { 
  images.push(images[i]); }

  randomizeImages();

// output images then hide them
var output = "<ol>"; 
for (var i = 0; i < 10; i++) { 
  output += "<li data-pattern='" + images[i].pattern + "'>";
  output += "<img src = '" + images[i].file + "'/>";
  output += "</li>";
}
output += "</ol>";
document.getElementById("container").innerHTML = output;
$("img").hide();

var guess1 = "";
var guess2 = "";
var count = 0;

$("li").click(function() {
  if ((count < 2) &&  ($(this).children("img").hasClass("face-up")) === false) {

    sendToSleeve($(this).data('pattern'));

    // increment guess count, show image, mark it as face up
    count++;
    $(this).children("img").show();
    $(this).children("img").addClass("face-up");
    
    //guess #1
    if (count === 1 ) { 
      guess1 = $(this).children("img").attr("src"); 
    }   
    
    //guess #2
    else { 
      guess2 = $(this).children("img").attr("src"); 
      
      // since it's the 2nd guess check for match
      if (guess1 === guess2) { 
        console.log("match");
        $("li").children("img[src='" + guess2 + "']").addClass("match");
      } 
      
      // else it's a miss
      else { 
        console.log("miss");
        setTimeout(function() {
          $("img").not(".match").hide();
          $("img").not(".match").removeClass("face-up");
        }, 1000);
      }
      
      // reset
      count = 0; 
      setTimeout(function() { console.clear(); }, 60000);      
    }
  }
});

function sendToSleeve(qwer) {
  var url = "http://127.0.0.1:62009/?"+qwer;
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true );
  xmlHttp.send( null );
}

$("li").hoverIntent(function() {
  if ($("#hover_hints").is(':checked')) {
    sendToSleeve($(this).data("pattern")); }
  },
  function () {
  // ignore hover-out
});

// randomize array of images
function randomizeImages(){
  Array.prototype.randomize = function()
  {
    var i = this.length, j, temp;
    while ( --i )
    {
      j = Math.floor( Math.random() * (i - 1) );
      temp = this[i];
      this[i] = this[j];
      this[j] = temp;
    }
  };
  
  images.randomize();
}