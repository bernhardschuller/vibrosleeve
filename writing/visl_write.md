# MGS Thesis
---
##Evaluation of human sensory input extension through a vibrotactile sensory substitution device by utilizing cognitive pattern recognition capabilities in a learning game
---
##Bewertung der Erweiterung menschlicher Sinneswahrnehmung durch ein vibrotaktiles sensorisches Substitutionsgerät unter Verwendung von kognitiver Mustererkennung in einem Lernspiel
---
#### Keywords
* Pattern Recognition
* Sensory Substitution
* Assisted Learning
* Force Feedback

# Overview
###Chapter 1
The promising possibilities of expanding on the human brain's pattern recognition capabilities
"Plug and Play into the human brain"
the normal five senses also operate on this ability

###Chapter 2
research that has been done on the topic(s)
(argumenting the feasibility and usefulness of the topic at hand)

###Chapter 3
creating a sensory substitution device
devising a plan for measuring sensory extension

####3a
the sensory substitution device (hardware)

####3b
software (library, dev app)

###Chapter 4
Measuring sensory extension using the VibroSleeve

####4a
the learning game

####4b
laboratory study with test subjects

###Chapter 5
summary

#1. Introduction
Cognitive Sciences have shown that the human brain uses pattern recognition to process various forms of sensory input, i.e. reading the words and letters in book, or even recognizing that the visual information that the eye has sent to the brain even contains letters. The function of the brain itself is mainly to process the incoming data in a way it can understand. 
In his [TED Talk](https://www.youtube.com/watch?v=4c1lqFXHvqI), neuro-scientist David Eagleman has presented a vest that produces vibrotactile stimulation on the wearer's torso to generate sensory input. In an experiment he describes, a deaf person has been trained on utilizing the vest by feeling audio signals which have been translated to the vest from a conversational partner, and after a few days of training the subject was able to identify spoken words in the conversation with relative ease.
Another example is a device called [Brainport](http://www.wicab.com/) for blind people, which is in principle a pair of glasses with a camera and a "straw" for the wearer which sends electrotactile signals to the tongue, by which the wearer can process visual input. For a list of more examples, see [].
Generally speaking, the application of using the brain's pattern recognition mechanism is not necessarily limited to replace missing senses, but it seems viable to use it to accompany, complement and enhance the usual five senses. 




## Index
### Hardware
#### A sensory substitution device called the 'VibroSleeve'
Meant to be worn on the forearm, equipped with 5 vibration motors, connected to an Arduino microcontroller via micro-USB.
![](images/sleeve2.jpg)

### Software
#### Control and Interface Library
A software library (DLL) written in C#. It provides the following tools:
	* Serial Port (COM) connection to the sleeve to switch motors on and off
	* Domain-specific language for describing vibration patterns in a concise way
	* Asynchronous thread scheduler that processes impulses from vibration patterns over time
	* TCP and HTTP listeners for receiving vibration patterns from any client application

#### Development Tool
This application provides a quick starting point for integrating the VibroSleeve into any client application. It lets you connect the sleeve, control the vibrations from a GUI and listens to incoming patterns on a network port.

### Chapter 3: Study
* Literature on the topics of sensory substition devices, cognitive pattern recognition and learning
* Engineering a method to measure the effects in question by using a learning game
* Results of a laboratory study using the method


## Paper Research
vibration motor
+	force feedback
+ tactile
+ pattern recognition
+ vibro-tactile
+ vibration patterns


Warum am Ärmel? Wegen Integriertbarkeit in andere Sachen. Die Hände sollen frei sein und mit diversen Handheld-Devices interagieren können. 
Die Hände selbst sind nicht der Empfänger dieses Musters. Der gewählte Platz ist der Unterarm, 
das Gehirn soll sich auch darauf einstellen dass die Sinnesquelle fix dort gelagert ist, unabhängig von den Händen. Und es ist der Ort, wo es gar nicht stört. [vor][^fn] [^x1]

[^fn]: [something](http://sport.orf.at)
[^x1]: [another thing](http://orf.at)


## Bla
VislPattern

A command language for triggering vibrations

ms:motors, ms:motors, ms:motors

ms: duration in milliseconds for this command part

milliseconds:motors
	timed sequences separated by colon (,)
	all motors stop (0) at end of command

multiple concurrent commands need to be merged!

Examples:
50:12, 50:0, 50:12
200:12345

VislScheduler



Spiele:
ereignis-gesteuert und radar-gesteuert

radar: orthogonal als basis, erst in weiterer stufe mit diagonalen
ereignisse: zB DOTA ulti reload


Spiele:
Counter-Strike (radar-effekt von wo man abgeschossen wird)

Dota: ability reload, ulti reload, tower wird angegriffen (radar)
	https://developer.steampowered.com/wiki/Dota_2_Workshop_Tools/Scripting/Using_CreateHTTPRequest

BroSquad: reload fertig, ... (fluff)

Asteroids-aehnliches Spiel mit Radar


## Markdown ##
* https://www.google.at/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=markdown+custom+css


# 1. VibroSleeve

## Device Requirements

## Microcontroller - Arduino

## Device 1
### Arduino specs
* Arduino Pro or Pro Mini
* ATmega328 (5V, 16MHz)
* Programmer: AVRISP mkii

## PC - Sleeve Interface ##
### Transmission Codes ####
Bitmask
<table border="1">
<tr>
	<td style="width:10em">Decimal Value</td>
	<td>128</td>
	<td>64</td>
	<td>32</td>
	<td>16</td>
	<td>8</td>
	<td>4</td>
	<td>2</td>
	<td>1</td>
</tr>
	<td>Interpretation</td>
	<td>Unused</td>
	<td>Unused</td>
	<td>0 = Stop, 1 = Start</td>
	<td>Motor 5</td>
	<td>Motor 4</td>
	<td>Motor 3</td>
	<td>Motor 2</td>
	<td>Motor 1</td>
</table>

<http://www.fiz-ix.com/2013/02/using-bytes-and-bitmasks-to-control-multiple-arduino-digital-outputs/>
<http://www.instructables.com/id/Using-Enums-as-Bitflags/>
<https://www.arduino.cc/en/Reference/BitRead>
<https://books.google.at/books?id=LiGw4IhN0RUC&pg=PA82&lpg=PA82&dq=arduino+bit+flags&source=bl&ots=7o8wTDHCl3&sig=Yee8hkdJGYihE-fTzL8Z_NvWWaI&hl=de&sa=X&ved=0ahUKEwjZsaST37fPAhWDbxQKHUG_CRQQ6AEIPzAF#v=onepage&q=arduino%20bit%20flags&f=false>

### Examples
                        <string>n:round-robin-100-40,100:1,40:12,100:2,40:23,100:3,40:34,100:4,40:45,100:5,40:51,100:1,40:12,100:2,40:23,100:3,40:34,100:4,40:45,100:5,40:51,100:1,40:12,100:2,40:23,100:3,40:34,100:4,40:45,100:5,40:51</string>
                        <string>n:zittern,140:12,140:34,140:12,140:34,140:12,140:34,140:12,140:34</string>
                        <string>n:schwappen,400:5,200:0,320:5,80:345,200:0,300:5,500:345</string>


### Receiver ###
#### TCP Socket Server ####
<https://msdn.microsoft.com/en-us/magazine/dn605876.aspx>
<https://msdn.microsoft.com/en-us/library/6y0e13d3(v=vs.110).aspx>
#### HTTP Server ####


# 2. Study

## What to measure

A subject can recognize a vibration pattern
The recognition can be in addition to another sensory input (picture) or by itself
Reaction time and accuracy of recognition will vary between individual subjects

## How to measure

Memory Game
Warmup - 5 minutes of playing Memory





## Software Requirements



## How to conduct the study
