﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using VibroSleeve.DevApp.Properties;
using VibroSleeve.Library.Connection;
using VibroSleeve.Library.Scheduling;
using VibroSleeve.Library.Vibration;

namespace VibroSleeve.DevApp
{
	public partial class Form1 : Form
	{
		private static SleeveConnection _sleeveConnection = null;
    private TcpClient _tcpClient = null;

    private static Scheduler _scheduler = null;
	  private static Thread _schedulerThread = null;

    private static TcpReceiver _tcpReceiver = null;
    private static Thread _tcpThread = null;

	  private static HttpReceiver _httpReceiver = null;
	  private static Thread _httpThread = null;


    public Form1()
		{
			InitializeComponent();

			textBoxSerialPort.Text = "COM4";
	    
		  StartScheduler();
		}

		#region Events

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			listViewHistory.Items.Clear();
			// AddHistoryItem("n:1m 200,200:1");
			// AddHistoryItem("n:all motors 200,200:12345");
			ImportHistory();
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			DisconnectFromSleeve();
			StopTcpReceiver();
			StopHttpReceiver();
			ExportHistory();
			base.OnClosing(e);
		}

		private void buttonOn_Click(object sender, EventArgs e)
		{
			if (checkBoxReset.Checked)
				_sleeveConnection.Send(false, 1, 2, 3, 4, 5);

			SendToSleeve(true);
		}

		private void buttonOff_Click(object sender, EventArgs e)
		{
			SendToSleeve(false);
		}

		private void linkLabelMotorsAll_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			checkBoxMotor1.Checked = checkBoxMotor2.Checked = checkBoxMotor3.Checked
				= checkBoxMotor4.Checked = checkBoxMotor5.Checked = true;
		}

		private void linkLabelMotorsNone_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			checkBoxMotor1.Checked = checkBoxMotor2.Checked = checkBoxMotor3.Checked
				= checkBoxMotor4.Checked = checkBoxMotor5.Checked = false;
		}

		private void buttonSerialConnect_Click(object sender, EventArgs e)
		{
			ConnectToSleeve();
		}

		private void buttonSerialDisconnect_Click(object sender, EventArgs e)
		{
			DisconnectFromSleeve();
		}

		private int tcpPort { get { return Int32.Parse(textBoxTcpPort.Text); } }

		private void buttonTCPStart_Click(object sender, EventArgs e)
		{
			StartTcpReceiver(tcpPort);
		}

		private void buttonTCPStop_Click(object sender, EventArgs e)
		{
			StopTcpReceiver();
		}

    private int httpPort { get { return Int32.Parse(textBoxHttpPort.Text); } }

    private void buttonHttpStart_Click(object sender, EventArgs e)
    {
      StartHttpReceiver(httpPort);
    }

    private void buttonHttpStop_Click(object sender, EventArgs e)
    {
      StopHttpReceiver();
    }

    private void textBoxPattern_KeyUp(object sender, KeyEventArgs e)
		{
			// As long as the AcceptButton property on the Form is set, this is not needed.
			/*
			if (e.KeyCode == Keys.Enter)
			{
			  SendMessage();
			}
			*/
		}

		private void linkLabelSendMessage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
		  SendMessage();
		}

    private void SendMessage(string message = null, bool addToHistory = true)
    {
	    var text = message ?? textBoxPattern.Text;

      if (_tcpReceiver != null)
        SendToTcpReceiver(text);

      if (_httpReceiver != null)
        SendToHttpReceiver(text);

      textBoxPattern.SelectAll();

	    if (addToHistory)
		    AddHistoryItem(text);
    }

		private void AddHistoryItem(string text)
		{
			var pattern = Pattern.Parse(text);
			if (pattern.Invalid)
				return;

			var listViewItem = new ListViewItem(pattern.Name);
			listViewItem.Tag = text;
			var patternText = String.Join(",", pattern.Impulses);
			listViewItem.SubItems.Add(patternText);

			listViewHistory.Items.Add(listViewItem);
		}

		private void ExportHistory()
		{
			var export = new System.Collections.Specialized.StringCollection();
			foreach (ListViewItem item in listViewHistory.Items)
				export.Add((string) item.Tag);
			Settings.Default["history"] = export;
			Settings.Default.Save();
		}

		private void ImportHistory()
		{
			var import = Settings.Default["history"] as System.Collections.Specialized.StringCollection;
			if (import != null)
			{
				foreach (var item in import)
					AddHistoryItem(item);
			}
		}

		private void buttonDemo_Click(object sender, EventArgs e)
		{
			_sleeveConnection.Send(Convert.ToChar(127));
		}

		private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			var itemsToRemove = listViewHistory.SelectedItems;
			foreach (ListViewItem item in itemsToRemove)
				listViewHistory.Items.Remove(item);
		}

		private void listViewHistory_DoubleClick(object sender, EventArgs e)
		{
			ListViewItem selectedItem = listViewHistory.SelectedItems.Count == 1
				? listViewHistory.SelectedItems[0]
				: null;

			if (selectedItem == null)
				return;

			SendMessage(selectedItem.SubItems[1].Text, false);
		}

		#endregion Events

		#region Scheduler

		private static void WriteToConsole(string output, ConsoleColor backgroundColor, ConsoleColor foregroundColor)
		{
			// Hold my color, I'm going in!
			var backgroundBefore = Console.BackgroundColor;
			var foregroundBefore = Console.ForegroundColor;

			Console.BackgroundColor = backgroundColor;
			Console.ForegroundColor = foregroundColor;
			Console.Write(output);
			Console.BackgroundColor = backgroundBefore;
			Console.ForegroundColor = foregroundBefore;
		}

		private static Random _random = new Random();

    private static void StartScheduler()
    {
      _scheduler = new Scheduler((power, motors) =>
      {
        var output = String.Format("{0}:{1}\t", power ? "On" : "Off", String.Join(", ", motors.ToArray()));
				WriteToConsole(output, ConsoleColor.DarkGreen, ConsoleColor.White);
				if (_sleeveConnection != null)
					_sleeveConnection.Send(power, motors);
      });
      _schedulerThread = new Thread(LoopScheduler);
      _schedulerThread.IsBackground = true;
      _schedulerThread.Start();
    }

    private static void LoopScheduler()
    {
      while (true)
      {
        _scheduler.Loop();
        Thread.Sleep(_random.Next(1, 5));
      }
    }

    public static void SendPatternToScheduler(string input)
    {
      try
      {
	      var console = String.Format(System.Environment.NewLine + DateTime.Now.ToString() + "\t" + input);
				WriteToConsole(console, ConsoleColor.Blue, ConsoleColor.White);

				var pattern = Pattern.Parse(input);
        if (!pattern.Invalid)
          _scheduler.Receive(pattern);
			}
      catch (Exception exception)
      {
				WriteToConsole(exception.ToString(), ConsoleColor.DarkRed, ConsoleColor.White);
      }
    }

    #endregion

    #region Sleeve Connection

    private void ConnectToSleeve()
		{
			if (_sleeveConnection == null)
			{
				_sleeveConnection = new SleeveConnection(textBoxSerialPort.Text, 9600);
			}
		}

		private void DisconnectFromSleeve()
		{
			if (_sleeveConnection != null)
			{
				_sleeveConnection.Dispose();
				_sleeveConnection = null;
			}
		}

		private void SendToSleeve(bool powerSwitch)
		{
			var motors = new List<int>();
			if (checkBoxMotor1.Checked)
				motors.Add(1);
			if (checkBoxMotor2.Checked)
				motors.Add(2);
			if (checkBoxMotor3.Checked)
				motors.Add(3);
			if (checkBoxMotor4.Checked)
				motors.Add(4);
			if (checkBoxMotor5.Checked)
				motors.Add(5);

			_sleeveConnection.Send(powerSwitch, motors.ToArray());
		}

    #endregion

    #region Tcp Receiver
    
    private static void StartTcpReceiver(int port)
		{
			if (_tcpReceiver == null)
			{
			  _tcpReceiver = new TcpReceiver(port, SendPatternToScheduler);
				ThreadStart ts = new ThreadStart(_tcpReceiver.Start);
				_tcpThread = new Thread(_tcpReceiver.Start);
				_tcpThread.IsBackground = true;
				_tcpThread.Start();
			}
		}

		private static void StopTcpReceiver()
		{
			if (_tcpReceiver != null)
			{
				_tcpThread.Abort();
				_tcpReceiver.Dispose();
				_tcpReceiver = null;
			}
		}

		private void SendToTcpReceiver(string message)
		{
			if (_tcpClient == null)
				_tcpClient = new TcpClient();

			var portHasChanged = _tcpClient.Client.RemoteEndPoint is IPEndPoint
				&& ((IPEndPoint) _tcpClient.Client.RemoteEndPoint).Port != tcpPort;
			if (portHasChanged)
			{
				_tcpClient.Close();
			}

			if (!_tcpClient.Connected)
			{
				_tcpClient.Connect("localhost", tcpPort);
			}
			
			byte[] buffer = Encoding.ASCII.GetBytes(message);
			_tcpClient.GetStream().Write(buffer, 0, buffer.Length);
			_tcpClient.GetStream().Flush();
		}

		#endregion

		#region Http Receiver

		private static void StartHttpReceiver(int port)
    {
      if (_httpReceiver == null)
      {
        _httpReceiver = new HttpReceiver(port, SendPatternToScheduler);
        _httpThread = new Thread(_httpReceiver.Start);
        _httpThread.IsBackground = true;
        _httpThread.Start();
      }
    }

    private static void StopHttpReceiver()
    {
      if (_httpReceiver != null)
      {
        _httpThread.Abort();
        _httpReceiver.Dispose();
        _httpReceiver = null;
      }
    }

    private void SendToHttpReceiver(string message)
    {
      var uriBuilder = new UriBuilder();
      uriBuilder.Host = "localhost";
      uriBuilder.Port = httpPort;
      uriBuilder.Query = message;
      
      var webRequest = WebRequest.Create(uriBuilder.Uri);
      
			// webRequest.BeginGetResponse(null, null);
	    var response = webRequest.GetResponse();
    }


		#endregion
	}
}
