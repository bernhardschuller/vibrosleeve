﻿namespace VibroSleeve.DevApp
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "1m 200",
            "200:1"}, -1);
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "all motors 200",
            "200:12345"}, -1);
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.buttonOn = new System.Windows.Forms.Button();
			this.buttonOff = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.checkBoxMotor1 = new System.Windows.Forms.CheckBox();
			this.checkBoxMotor2 = new System.Windows.Forms.CheckBox();
			this.checkBoxMotor3 = new System.Windows.Forms.CheckBox();
			this.checkBoxMotor4 = new System.Windows.Forms.CheckBox();
			this.checkBoxMotor5 = new System.Windows.Forms.CheckBox();
			this.linkLabelMotorsNone = new System.Windows.Forms.LinkLabel();
			this.linkLabelMotorsAll = new System.Windows.Forms.LinkLabel();
			this.checkBoxReset = new System.Windows.Forms.CheckBox();
			this.buttonDemo = new System.Windows.Forms.Button();
			this.textBoxSerialPort = new System.Windows.Forms.TextBox();
			this.buttonSerialConnect = new System.Windows.Forms.Button();
			this.buttonSerialDisconnect = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.buttonTCPStop = new System.Windows.Forms.Button();
			this.buttonTCPStart = new System.Windows.Forms.Button();
			this.textBoxTcpPort = new System.Windows.Forms.TextBox();
			this.textBoxPattern = new System.Windows.Forms.TextBox();
			this.linkLabelSendMessage = new System.Windows.Forms.LinkLabel();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.buttonHttpStop = new System.Windows.Forms.Button();
			this.buttonHttpStart = new System.Windows.Forms.Button();
			this.textBoxHttpPort = new System.Windows.Forms.TextBox();
			this.tabControlReceiver = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.listViewHistory = new System.Windows.Forms.ListView();
			this.linkLabelHistoryRemove = new System.Windows.Forms.LinkLabel();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.groupBox1.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.tabControlReceiver.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonOn
			// 
			this.buttonOn.Location = new System.Drawing.Point(82, 185);
			this.buttonOn.Name = "buttonOn";
			this.buttonOn.Size = new System.Drawing.Size(92, 58);
			this.buttonOn.TabIndex = 0;
			this.buttonOn.Text = "ON";
			this.buttonOn.UseVisualStyleBackColor = true;
			this.buttonOn.Click += new System.EventHandler(this.buttonOn_Click);
			// 
			// buttonOff
			// 
			this.buttonOff.Location = new System.Drawing.Point(82, 266);
			this.buttonOff.Name = "buttonOff";
			this.buttonOff.Size = new System.Drawing.Size(92, 32);
			this.buttonOff.TabIndex = 4;
			this.buttonOff.Text = "OFF";
			this.buttonOff.UseVisualStyleBackColor = true;
			this.buttonOff.Click += new System.EventHandler(this.buttonOff_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.flowLayoutPanel1);
			this.groupBox1.Controls.Add(this.linkLabelMotorsNone);
			this.groupBox1.Controls.Add(this.linkLabelMotorsAll);
			this.groupBox1.Controls.Add(this.checkBoxReset);
			this.groupBox1.Controls.Add(this.buttonDemo);
			this.groupBox1.Controls.Add(this.buttonOff);
			this.groupBox1.Controls.Add(this.buttonOn);
			this.groupBox1.Location = new System.Drawing.Point(882, 220);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(302, 303);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Motor Control";
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Controls.Add(this.checkBoxMotor1);
			this.flowLayoutPanel1.Controls.Add(this.checkBoxMotor2);
			this.flowLayoutPanel1.Controls.Add(this.checkBoxMotor3);
			this.flowLayoutPanel1.Controls.Add(this.checkBoxMotor4);
			this.flowLayoutPanel1.Controls.Add(this.checkBoxMotor5);
			this.flowLayoutPanel1.Location = new System.Drawing.Point(82, 25);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(94, 154);
			this.flowLayoutPanel1.TabIndex = 16;
			// 
			// checkBoxMotor1
			// 
			this.checkBoxMotor1.AutoSize = true;
			this.checkBoxMotor1.Location = new System.Drawing.Point(3, 3);
			this.checkBoxMotor1.Name = "checkBoxMotor1";
			this.checkBoxMotor1.Size = new System.Drawing.Size(89, 24);
			this.checkBoxMotor1.TabIndex = 9;
			this.checkBoxMotor1.Text = "Motor 1";
			this.checkBoxMotor1.UseVisualStyleBackColor = true;
			// 
			// checkBoxMotor2
			// 
			this.checkBoxMotor2.AutoSize = true;
			this.checkBoxMotor2.Location = new System.Drawing.Point(3, 33);
			this.checkBoxMotor2.Name = "checkBoxMotor2";
			this.checkBoxMotor2.Size = new System.Drawing.Size(89, 24);
			this.checkBoxMotor2.TabIndex = 10;
			this.checkBoxMotor2.Text = "Motor 2";
			this.checkBoxMotor2.UseVisualStyleBackColor = true;
			// 
			// checkBoxMotor3
			// 
			this.checkBoxMotor3.AutoSize = true;
			this.checkBoxMotor3.Location = new System.Drawing.Point(3, 63);
			this.checkBoxMotor3.Name = "checkBoxMotor3";
			this.checkBoxMotor3.Size = new System.Drawing.Size(89, 24);
			this.checkBoxMotor3.TabIndex = 11;
			this.checkBoxMotor3.Text = "Motor 3";
			this.checkBoxMotor3.UseVisualStyleBackColor = true;
			// 
			// checkBoxMotor4
			// 
			this.checkBoxMotor4.AutoSize = true;
			this.checkBoxMotor4.Location = new System.Drawing.Point(3, 93);
			this.checkBoxMotor4.Name = "checkBoxMotor4";
			this.checkBoxMotor4.Size = new System.Drawing.Size(89, 24);
			this.checkBoxMotor4.TabIndex = 12;
			this.checkBoxMotor4.Text = "Motor 4";
			this.checkBoxMotor4.UseVisualStyleBackColor = true;
			// 
			// checkBoxMotor5
			// 
			this.checkBoxMotor5.AutoSize = true;
			this.checkBoxMotor5.Location = new System.Drawing.Point(3, 123);
			this.checkBoxMotor5.Name = "checkBoxMotor5";
			this.checkBoxMotor5.Size = new System.Drawing.Size(89, 24);
			this.checkBoxMotor5.TabIndex = 13;
			this.checkBoxMotor5.Text = "Motor 5";
			this.checkBoxMotor5.UseVisualStyleBackColor = true;
			// 
			// linkLabelMotorsNone
			// 
			this.linkLabelMotorsNone.AutoSize = true;
			this.linkLabelMotorsNone.Location = new System.Drawing.Point(190, 89);
			this.linkLabelMotorsNone.Name = "linkLabelMotorsNone";
			this.linkLabelMotorsNone.Size = new System.Drawing.Size(45, 20);
			this.linkLabelMotorsNone.TabIndex = 15;
			this.linkLabelMotorsNone.TabStop = true;
			this.linkLabelMotorsNone.Text = "none";
			this.linkLabelMotorsNone.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelMotorsNone_LinkClicked);
			// 
			// linkLabelMotorsAll
			// 
			this.linkLabelMotorsAll.AutoSize = true;
			this.linkLabelMotorsAll.Location = new System.Drawing.Point(20, 89);
			this.linkLabelMotorsAll.Name = "linkLabelMotorsAll";
			this.linkLabelMotorsAll.Size = new System.Drawing.Size(42, 20);
			this.linkLabelMotorsAll.TabIndex = 14;
			this.linkLabelMotorsAll.TabStop = true;
			this.linkLabelMotorsAll.Text = "_all_";
			this.linkLabelMotorsAll.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelMotorsAll_LinkClicked);
			// 
			// checkBoxReset
			// 
			this.checkBoxReset.AutoSize = true;
			this.checkBoxReset.Checked = true;
			this.checkBoxReset.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxReset.Location = new System.Drawing.Point(195, 203);
			this.checkBoxReset.Name = "checkBoxReset";
			this.checkBoxReset.Size = new System.Drawing.Size(91, 24);
			this.checkBoxReset.TabIndex = 14;
			this.checkBoxReset.Text = "override";
			this.checkBoxReset.UseVisualStyleBackColor = true;
			// 
			// buttonDemo
			// 
			this.buttonDemo.Location = new System.Drawing.Point(195, 266);
			this.buttonDemo.Name = "buttonDemo";
			this.buttonDemo.Size = new System.Drawing.Size(96, 29);
			this.buttonDemo.TabIndex = 14;
			this.buttonDemo.Text = "DEMO";
			this.buttonDemo.UseVisualStyleBackColor = true;
			this.buttonDemo.Click += new System.EventHandler(this.buttonDemo_Click);
			// 
			// textBoxSerialPort
			// 
			this.textBoxSerialPort.Location = new System.Drawing.Point(6, 28);
			this.textBoxSerialPort.Name = "textBoxSerialPort";
			this.textBoxSerialPort.Size = new System.Drawing.Size(100, 26);
			this.textBoxSerialPort.TabIndex = 9;
			// 
			// buttonSerialConnect
			// 
			this.buttonSerialConnect.Location = new System.Drawing.Point(123, 24);
			this.buttonSerialConnect.Name = "buttonSerialConnect";
			this.buttonSerialConnect.Size = new System.Drawing.Size(100, 35);
			this.buttonSerialConnect.TabIndex = 10;
			this.buttonSerialConnect.Text = "Connect";
			this.buttonSerialConnect.UseVisualStyleBackColor = true;
			this.buttonSerialConnect.Click += new System.EventHandler(this.buttonSerialConnect_Click);
			// 
			// buttonSerialDisconnect
			// 
			this.buttonSerialDisconnect.Location = new System.Drawing.Point(242, 23);
			this.buttonSerialDisconnect.Name = "buttonSerialDisconnect";
			this.buttonSerialDisconnect.Size = new System.Drawing.Size(112, 35);
			this.buttonSerialDisconnect.TabIndex = 11;
			this.buttonSerialDisconnect.Text = "Disconnect";
			this.buttonSerialDisconnect.UseVisualStyleBackColor = true;
			this.buttonSerialDisconnect.Click += new System.EventHandler(this.buttonSerialDisconnect_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.textBoxSerialPort);
			this.groupBox2.Controls.Add(this.buttonSerialDisconnect);
			this.groupBox2.Controls.Add(this.buttonSerialConnect);
			this.groupBox2.Location = new System.Drawing.Point(12, 47);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(360, 86);
			this.groupBox2.TabIndex = 12;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Sleeve Connection (Serial Port)";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.buttonTCPStop);
			this.groupBox3.Controls.Add(this.buttonTCPStart);
			this.groupBox3.Controls.Add(this.textBoxTcpPort);
			this.groupBox3.Location = new System.Drawing.Point(6, 6);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(360, 72);
			this.groupBox3.TabIndex = 13;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Tcp Port";
			// 
			// buttonTCPStop
			// 
			this.buttonTCPStop.Location = new System.Drawing.Point(242, 20);
			this.buttonTCPStop.Name = "buttonTCPStop";
			this.buttonTCPStop.Size = new System.Drawing.Size(112, 35);
			this.buttonTCPStop.TabIndex = 3;
			this.buttonTCPStop.Text = "Stop";
			this.buttonTCPStop.UseVisualStyleBackColor = true;
			this.buttonTCPStop.Click += new System.EventHandler(this.buttonTCPStop_Click);
			// 
			// buttonTCPStart
			// 
			this.buttonTCPStart.Location = new System.Drawing.Point(120, 20);
			this.buttonTCPStart.Name = "buttonTCPStart";
			this.buttonTCPStart.Size = new System.Drawing.Size(100, 35);
			this.buttonTCPStart.TabIndex = 2;
			this.buttonTCPStart.Text = "Start";
			this.buttonTCPStart.UseVisualStyleBackColor = true;
			this.buttonTCPStart.Click += new System.EventHandler(this.buttonTCPStart_Click);
			// 
			// textBoxTcpPort
			// 
			this.textBoxTcpPort.Location = new System.Drawing.Point(6, 25);
			this.textBoxTcpPort.Name = "textBoxTcpPort";
			this.textBoxTcpPort.Size = new System.Drawing.Size(100, 26);
			this.textBoxTcpPort.TabIndex = 0;
			this.textBoxTcpPort.Text = "62009";
			// 
			// textBoxPattern
			// 
			this.textBoxPattern.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxPattern.Location = new System.Drawing.Point(12, 159);
			this.textBoxPattern.Name = "textBoxPattern";
			this.textBoxPattern.Size = new System.Drawing.Size(1175, 26);
			this.textBoxPattern.TabIndex = 4;
			this.textBoxPattern.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxPattern_KeyUp);
			// 
			// linkLabelSendMessage
			// 
			this.linkLabelSendMessage.AutoSize = true;
			this.linkLabelSendMessage.Location = new System.Drawing.Point(12, 136);
			this.linkLabelSendMessage.Name = "linkLabelSendMessage";
			this.linkLabelSendMessage.Size = new System.Drawing.Size(103, 20);
			this.linkLabelSendMessage.TabIndex = 14;
			this.linkLabelSendMessage.TabStop = true;
			this.linkLabelSendMessage.Text = "Send Pattern";
			this.linkLabelSendMessage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSendMessage_LinkClicked);
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.buttonHttpStop);
			this.groupBox4.Controls.Add(this.buttonHttpStart);
			this.groupBox4.Controls.Add(this.textBoxHttpPort);
			this.groupBox4.Location = new System.Drawing.Point(6, 6);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(360, 72);
			this.groupBox4.TabIndex = 14;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Http Port";
			// 
			// buttonHttpStop
			// 
			this.buttonHttpStop.Location = new System.Drawing.Point(242, 20);
			this.buttonHttpStop.Name = "buttonHttpStop";
			this.buttonHttpStop.Size = new System.Drawing.Size(112, 35);
			this.buttonHttpStop.TabIndex = 3;
			this.buttonHttpStop.Text = "Stop";
			this.buttonHttpStop.UseVisualStyleBackColor = true;
			this.buttonHttpStop.Click += new System.EventHandler(this.buttonHttpStop_Click);
			// 
			// buttonHttpStart
			// 
			this.buttonHttpStart.Location = new System.Drawing.Point(120, 20);
			this.buttonHttpStart.Name = "buttonHttpStart";
			this.buttonHttpStart.Size = new System.Drawing.Size(100, 35);
			this.buttonHttpStart.TabIndex = 2;
			this.buttonHttpStart.Text = "Start";
			this.buttonHttpStart.UseVisualStyleBackColor = true;
			this.buttonHttpStart.Click += new System.EventHandler(this.buttonHttpStart_Click);
			// 
			// textBoxHttpPort
			// 
			this.textBoxHttpPort.Location = new System.Drawing.Point(6, 25);
			this.textBoxHttpPort.Name = "textBoxHttpPort";
			this.textBoxHttpPort.Size = new System.Drawing.Size(100, 26);
			this.textBoxHttpPort.TabIndex = 0;
			this.textBoxHttpPort.Text = "62009";
			// 
			// tabControlReceiver
			// 
			this.tabControlReceiver.Controls.Add(this.tabPage1);
			this.tabControlReceiver.Controls.Add(this.tabPage2);
			this.tabControlReceiver.Location = new System.Drawing.Point(378, 12);
			this.tabControlReceiver.Name = "tabControlReceiver";
			this.tabControlReceiver.SelectedIndex = 0;
			this.tabControlReceiver.Size = new System.Drawing.Size(396, 125);
			this.tabControlReceiver.TabIndex = 15;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.groupBox4);
			this.tabPage1.Location = new System.Drawing.Point(4, 29);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(388, 92);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "HTTP";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.groupBox3);
			this.tabPage2.Location = new System.Drawing.Point(4, 29);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(388, 92);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "TCP";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// groupBox5
			// 
			this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox5.Controls.Add(this.linkLabelHistoryRemove);
			this.groupBox5.Controls.Add(this.listViewHistory);
			this.groupBox5.Location = new System.Drawing.Point(12, 210);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(864, 576);
			this.groupBox5.TabIndex = 17;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Session History";
			// 
			// listViewHistory
			// 
			this.listViewHistory.Activation = System.Windows.Forms.ItemActivation.OneClick;
			this.listViewHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.listViewHistory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
			this.listViewHistory.FullRowSelect = true;
			this.listViewHistory.GridLines = true;
			this.listViewHistory.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2});
			this.listViewHistory.Location = new System.Drawing.Point(6, 47);
			this.listViewHistory.Name = "listViewHistory";
			this.listViewHistory.Size = new System.Drawing.Size(852, 523);
			this.listViewHistory.TabIndex = 0;
			this.listViewHistory.UseCompatibleStateImageBehavior = false;
			this.listViewHistory.View = System.Windows.Forms.View.Details;
			this.listViewHistory.DoubleClick += new System.EventHandler(this.listViewHistory_DoubleClick);
			// 
			// linkLabelHistoryRemove
			// 
			this.linkLabelHistoryRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabelHistoryRemove.AutoSize = true;
			this.linkLabelHistoryRemove.Location = new System.Drawing.Point(797, 10);
			this.linkLabelHistoryRemove.Name = "linkLabelHistoryRemove";
			this.linkLabelHistoryRemove.Size = new System.Drawing.Size(61, 20);
			this.linkLabelHistoryRemove.TabIndex = 1;
			this.linkLabelHistoryRemove.TabStop = true;
			this.linkLabelHistoryRemove.Text = "remove";
			this.linkLabelHistoryRemove.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Name";
			this.columnHeader1.Width = 252;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Pattern";
			this.columnHeader2.Width = 1224;
			// 
			// Form1
			// 
			this.AcceptButton = this.linkLabelSendMessage;
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1200, 814);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.tabControlReceiver);
			this.Controls.Add(this.linkLabelSendMessage);
			this.Controls.Add(this.textBoxPattern);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "VibroSleeve Dev App";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.tabControlReceiver.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button buttonOn;
		private System.Windows.Forms.Button buttonOff;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox checkBoxMotor5;
		private System.Windows.Forms.CheckBox checkBoxMotor4;
		private System.Windows.Forms.CheckBox checkBoxMotor3;
		private System.Windows.Forms.CheckBox checkBoxMotor2;
		private System.Windows.Forms.CheckBox checkBoxMotor1;
		private System.Windows.Forms.TextBox textBoxSerialPort;
		private System.Windows.Forms.Button buttonSerialConnect;
		private System.Windows.Forms.Button buttonSerialDisconnect;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button buttonTCPStop;
		private System.Windows.Forms.Button buttonTCPStart;
		private System.Windows.Forms.TextBox textBoxTcpPort;
		private System.Windows.Forms.TextBox textBoxPattern;
		private System.Windows.Forms.Button buttonDemo;
		private System.Windows.Forms.CheckBox checkBoxReset;
		private System.Windows.Forms.LinkLabel linkLabelMotorsNone;
		private System.Windows.Forms.LinkLabel linkLabelMotorsAll;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.LinkLabel linkLabelSendMessage;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.Button buttonHttpStop;
    private System.Windows.Forms.Button buttonHttpStart;
    private System.Windows.Forms.TextBox textBoxHttpPort;
		private System.Windows.Forms.TabControl tabControlReceiver;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.LinkLabel linkLabelHistoryRemove;
		private System.Windows.Forms.ListView listViewHistory;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
	}
}

