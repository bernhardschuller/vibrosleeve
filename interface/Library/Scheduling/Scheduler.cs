﻿using System;
using System.Collections.Generic;
using System.Linq;
using VibroSleeve.Library.Vibration;

namespace VibroSleeve.Library.Scheduling
{
	public class Scheduler
	{
		public DateTime SessionStart = DateTime.Now;
		public DateTime LastProcessing = DateTime.MinValue;

		public delegate void SleeveSignalDelegate(bool power, params int[] motors);

		protected readonly SleeveSignalDelegate _sleeveSignal;

		internal TimeSlotCollection TimeSlots = new TimeSlotCollection();
		
		public bool[] MotorsActive = new bool[5];
		protected bool[] MotorsDiff = new bool[5];

		public Scheduler(SleeveSignalDelegate sleeveSignal)
		{
			_sleeveSignal = sleeveSignal;
		}

		public void Receive(Pattern pattern)
		{
			TimeSlots.AddPattern(pattern);
		}

		public void Loop()
		{
			var now = DateTime.Now;			
			// var bygone = LastProcessing.Subtract(now); // performance debugging
			LastProcessing = now;

			var motors = ProcessUntil(now);
			UpdateMotors(motors);
		}

		protected internal int[] ProcessUntil(DateTime time)
		{
			TimeSlots.UpdatePresent(time);
			TimeSlots.RemovePast(time);

			var motors = TimeSlots.GetActiveMotors();
			return motors;
		}

		protected void UpdateMotors(int[] motors)
		{
			// Check if there is a difference to the current motors
			// and only send an update if needed.

			for (int i = 0; i < 5; i++)
			{
				MotorsDiff[i] = motors.Contains(i + 1);
			}

			for (int i = 0; i < 5; i++)
			{
				if (MotorsDiff[i] != MotorsActive[i])
					_sleeveSignal(MotorsDiff[i], i + 1);

				MotorsActive[i] = MotorsDiff[i];
			}
		}
	}	
}
