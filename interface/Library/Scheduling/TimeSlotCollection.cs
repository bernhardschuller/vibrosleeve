﻿using System;
using System.Collections.Generic;
using System.Linq;
using VibroSleeve.Library.Vibration;

namespace VibroSleeve.Library.Scheduling
{
	internal class TimeSlotCollection
	{
		public HashSet<TimeSlot> Future = new HashSet<TimeSlot>();
	  public HashSet<TimeSlot> Present = new HashSet<TimeSlot>();
		private readonly object _lockObject = new object();

    public void AddPattern(Pattern pattern, DateTime? beginTime = null)
    {
      var timeSlots = pattern.AsTimeSlots(beginTime);
      AddTimeSlots(timeSlots);
		}

		public void AddTimeSlots(IEnumerable<TimeSlot> timeSlots)
		{
			lock (_lockObject)
			{
				foreach (var timeSlot in timeSlots)
					Future.Add(timeSlot);
			}
		}

    public void UpdatePresentAndRemovePast(DateTime? time = null)
    {
      if (!time.HasValue)
        time = DateTime.Now;

      UpdatePresent(time.Value);
      RemovePast(time.Value);
    }

		public void UpdatePresent(DateTime begunUntil)
		{
			TimeSlot[] timeSlots;
			lock (_lockObject)
			{
				timeSlots = Future.Where(ts => ts.Begin <= begunUntil).ToArray();
			  foreach (var ts in timeSlots)
			  {
			    Future.Remove(ts);
			    Present.Add(ts);
			  }
			}
		}

		public void RemovePast(DateTime endedUntil)
		{
			TimeSlot[] timeSlots;
			lock (_lockObject)
			{
				timeSlots = Present.Where(ts => ts.End < endedUntil).ToArray();
				foreach (var ts in timeSlots)
					Present.Remove(ts);
			}
		}

		public int[] GetActiveMotors()
		{
			var motors = new HashSet<int>();
			lock (_lockObject)
			{
				foreach (var timeSlot in Present)
					foreach (var m in timeSlot.Motors)
						motors.Add(m);
			}

			return motors.OrderBy(m => m).ToArray();
		}
	}
}
