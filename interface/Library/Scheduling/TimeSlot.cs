﻿using System;
using System.Collections.Generic;
using System.Linq;
using VibroSleeve.Library.Vibration;

namespace VibroSleeve.Library.Scheduling
{
	internal struct TimeSlot
	{
		public DateTime Begin;
		public DateTime End;
		public int[] Motors;

		public TimeSlot(int durationMs, params int[] motors)
		{
			Begin = DateTime.Now;
			End = Begin.AddMilliseconds(durationMs);
			Motors = motors;
		}

		public TimeSlot(DateTime begin, DateTime end, params int[] motors)
		{
			Begin = begin;
			End = end;
			Motors = motors;
		}
	}

  internal static class PatternTimeSlotConversionExtension
  {
    public static TimeSlot[] AsTimeSlots(this Pattern pattern, DateTime? beginTime = null)
    {
      if (!beginTime.HasValue)
        beginTime = DateTime.Now;

      var timeSlots = new List<TimeSlot>();
      foreach (var impulse in pattern.Impulses)
      {
        var t = new TimeSlot(beginTime.Value, beginTime.Value.AddMilliseconds(impulse.DurationMS), impulse.Motors);
        beginTime = t.End.AddMilliseconds(1);
        timeSlots.Add(t);
      }

      return timeSlots.ToArray();
    }
  }
}
