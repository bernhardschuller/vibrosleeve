﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace VibroSleeve.Library.Connection
{
	public class HttpReceiver : IDisposable
	{
		public delegate void OnReceiveAction(string input);

		private readonly HttpListener _httpListener;
		private readonly OnReceiveAction _onReceive;

		public HttpReceiver(int port, OnReceiveAction onReceive)
		{
			_httpListener = new HttpListener();
			_httpListener.Prefixes.Add($"http://*:{port}/");
			_httpListener.Start();

			_onReceive = onReceive;
		}

		public void Dispose()
		{
      _httpListener.Stop();
      _httpListener.Close();
    }

    public void Start()
		{
		  _httpListener.Start();

		  while (true)
      {
        HttpListenerContext context = _httpListener.GetContext();
        var query = context.Request.Url.Query;
				var fixedQuery = query.Replace("?", "").Replace("%20", "");

				var response = context.Response;
	      response.StatusCode = 200;
	      response.KeepAlive = false;
				response.Headers.Add("Pattern", fixedQuery);
				response.Headers.Add("Access-Control-Allow-Origin", "*");
				context.Response.Close();

        _onReceive(fixedQuery);
      }
		}
	}
}
