using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace VibroSleeve.Library.Connection
{
	public class TcpReceiver : IDisposable
	{
		public delegate void OnReceiveAction(string input);

		private readonly Socket _socket;
		private readonly IPEndPoint _ipEndPoint;
		private readonly OnReceiveAction _onReceive;

		public TcpReceiver(int port, OnReceiveAction onReceive)
		{
			_socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
			_ipEndPoint = new IPEndPoint(IPAddress.Loopback, port);
			_socket.Bind(_ipEndPoint);
			_onReceive = onReceive;
		}

		protected bool StopSignal { get; set; }

		public void Start()
		{
			byte[] bytes;

			_socket.Listen(1);

			while (!StopSignal)
			{
				var client = _socket.Accept();
				while (!StopSignal)
				{
					bytes = new byte[1024];
					int bytesRec = client.Receive(bytes);

					if (bytesRec == 0)
						continue;

					var received = Encoding.ASCII.GetString(bytes, 0, bytesRec);
					_onReceive(received);
				}

				client.Shutdown(SocketShutdown.Both);
				client.Close();
			}

			StopSignal = false;
		}

		public void Dispose()
		{
			_socket.Close();
			_socket.Dispose();
		}
	}
}
