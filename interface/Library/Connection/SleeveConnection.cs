using System;
using System.IO.Ports;
using System.Linq;

namespace VibroSleeve.Library.Connection
{
	public class SleeveConnection : IDisposable
	{
		public string PortName { get; private set; }
		public int BaudRate { get; private set; }

		private readonly SerialPort _port;

		public SleeveConnection(string portName, int baudRate)
		{
			PortName = portName;
			BaudRate = baudRate;

			_port = new SerialPort(PortName, BaudRate);
			_port.Open();
		}

		public void Send(char byteValue)
		{
			_port.Write(byteValue.ToString());
		}

		public void Send(bool power, params int[] motors)
		{
			Bitmask bitmask = Bitmask.PowerOff;
			if (power)
				bitmask |= Bitmask.PowerOn;

			if (motors.Contains(1))
				bitmask |= Bitmask.Motor1;

			if (motors.Contains(2))
				bitmask |= Bitmask.Motor2;

			if (motors.Contains(3))
				bitmask |= Bitmask.Motor3;

			if (motors.Contains(4))
				bitmask |= Bitmask.Motor4;

			if (motors.Contains(5))
				bitmask |= Bitmask.Motor5;

			var transmission = (char) bitmask;
			Send(transmission);
		}
		
		public void Dispose()
		{
			_port.Close();
		}

		[Flags]
		enum Bitmask
		{
			PowerOff = 0,
			PowerOn = 1,
			Motor1 = 2,
			Motor2 = 4,
			Motor3 = 8,
			Motor4 = 16,
			Motor5 = 32,
			Unused1 = 64,
			Unused2 = 128
		}
	}
}
