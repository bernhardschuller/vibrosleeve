﻿using System.Linq;

namespace VibroSleeve.Library.Vibration
{
	public struct Impulse
	{
		public readonly int DurationMS;
		public readonly int[] Motors;

		public Impulse(int durationMS, int[] motors)
		{
			DurationMS = durationMS;
			Motors = motors.Distinct().OrderBy(m => m).ToArray();
		}

		public override string ToString()
		{
			var motorsAsString = Motors.Length == 0
				? "0"
				: Motors.Aggregate("", (current, motor) => current + motor);

			var s = $"{DurationMS}:{motorsAsString}";
			return s;
		}


	}
}
