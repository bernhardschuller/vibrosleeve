using System;
using System.Collections.Generic;
using System.Linq;

namespace VibroSleeve.Library.Vibration
{
	public class Pattern
	{
		public string Name { get; protected set; }
		public string OriginalText { get; protected set; }
		public Impulse[] Impulses { get; protected set; }
		public bool Invalid { get; protected set; }

		protected Pattern()
		{
		}

		public static Pattern Parse(string text)
		{
			var pattern = new Pattern();
			pattern.OriginalText = text;
			try
			{
				pattern.Assemble();
			}
			catch
			{
				// Gotta catch 'em all!
				pattern.Invalid = true;
			}
			return pattern;
		}

		private void Assemble()
		{
			List<Impulse> commands = new List<Impulse>();
			var textParts = OriginalText.Trim().Split(',');
			foreach (var textPart in textParts)
			{
				var before = BeforeColon(textPart);
				var after = AfterColon(textPart);

				int number;
				if (Int32.TryParse(before, out number))
				{
					if (number <= 0)
						throw new PatternParseException();

					var digits = ParseDigits(after);
					var command = new Impulse(number, digits);
					commands.Add(command);
				}
				else if (before == "n")
				{
					Name = after;
				}
				else
				{
					throw new PatternParseException();
				}
			}

			Impulses = commands.ToArray();
		}

		internal static string BeforeColon(string text)
		{
			var commaIndex = text.IndexOf(':');
			var before = text.Substring(0, commaIndex).Trim();
			return before;
		}

		internal static string AfterColon(string text)
		{
			var commaIndex = text.IndexOf(':');
			var after = text.Substring(commaIndex + 1, text.Length - commaIndex - 1).Trim();
			return after;
		}

		internal static int[] ParseDigits(string text)
		{
			var digits = text
				.Distinct()
				.Where(c => c != '0')
				.Select(c => Int32.Parse(c.ToString()))
				.ToArray();
			return digits;
		}

		public class PatternParseException : Exception
		{
			public PatternParseException() : base()
			{
				
			}
		}
	}
}
