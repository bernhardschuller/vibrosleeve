﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VibroSleeve.Library.Scheduling;
using VibroSleeve.Library.Vibration;

namespace UnitTests
{
	/// <summary>
	/// These unit tests work in real-time. They should pass under typical CPU power and CPU load.
	/// </summary>
	[TestClass]
	public class SchedulerTests
	{
		[TestMethod]
		public void Scheduler_test1()
		{
			Scheduler.SleeveSignalDelegate sleeveSignal = delegate(bool power, int[] motors)
			{
				// ignore
			};

			var scheduler = new Scheduler(sleeveSignal);
			scheduler.Receive(Pattern.Parse("100:1, 100:0, 100:1"));

			System.Threading.Thread.Sleep(1);
			scheduler.Loop();
			CollectionAssert.AreEqual(new [] {true, false, false, false, false}, scheduler.MotorsActive);

			System.Threading.Thread.Sleep(100);
			scheduler.Loop();
			CollectionAssert.AreEqual(new[] { false, false, false, false, false }, scheduler.MotorsActive);

			System.Threading.Thread.Sleep(100);
			scheduler.Loop();
			CollectionAssert.AreEqual(new[] { true, false, false, false, false }, scheduler.MotorsActive);
		}
	}
}
