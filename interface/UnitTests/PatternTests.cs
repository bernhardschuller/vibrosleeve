﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VibroSleeve.Library.Vibration;

namespace UnitTests
{
	[TestClass]
	public class PatternTests
	{
		private void AssertImpulses(Pattern pattern, int impulseCount, params string[] impulseStrings)
		{
			Assert.AreEqual(impulseCount, pattern.Impulses.Length);
			Assert.AreEqual(impulseCount, impulseStrings.Length);
			for (int i = 0; i < impulseCount; i++)
			{
				var expectedString = impulseStrings[i];
				var actualString = pattern.Impulses[i].ToString();
				Assert.AreEqual(expectedString, actualString);
			}
		}

		[TestMethod]
		public void ImpromptuPatterns()
		{
			AssertImpulses(Pattern.Parse("50:12, 50:0, 50:12"), 3, "50:12", "50:0", "50:12");
			AssertImpulses(Pattern.Parse("50 : 12, 50:0 ,50:12"), 3, "50:12", "50:0", "50:12");
			AssertImpulses(Pattern.Parse("200:12345"), 1, "200:12345");
		}

		[TestMethod]
		public void NamedPatterns()
		{
		  var pattern = Pattern.Parse("n:Reload, 123:0, 6000:4, 20:0");
      AssertImpulses(pattern, 3, "123:0", "6000:4", "20:0");
		  Assert.AreEqual("Reload", pattern.Name);
		}

		[TestMethod]
		public void InvalidPatterns()
		{
			Assert.IsTrue(Pattern.Parse("invalid").Invalid);
			Assert.IsTrue(Pattern.Parse("50::12").Invalid);
			Assert.IsTrue(Pattern.Parse("0:1").Invalid);
			Assert.IsTrue(Pattern.Parse("0:0").Invalid);
			Assert.IsTrue(Pattern.Parse("0:0:0").Invalid);
			Assert.IsTrue(Pattern.Parse("").Invalid);
			Assert.IsTrue(Pattern.Parse(" ").Invalid);
			Assert.IsTrue(Pattern.Parse(" : ").Invalid);
			Assert.IsTrue(Pattern.Parse(" , ").Invalid);
			Assert.IsTrue(Pattern.Parse(",,").Invalid);
		}
		
		[TestMethod]
		public void ColonSeparators()
		{
			Assert.AreEqual("before", Pattern.BeforeColon("before:after"));
			Assert.AreEqual("after", Pattern.AfterColon("before:after"));
		}

		[TestMethod]
		public void DigitsParser()
		{
			CollectionAssert.AreEqual(new[] {1, 2, 3, 4, 5}, Pattern.ParseDigits("12345"));
			CollectionAssert.AreEqual(new[] {1}, Pattern.ParseDigits("111"));
			CollectionAssert.AreEqual(new int[0], Pattern.ParseDigits("0"));
			CollectionAssert.AreEqual(new int[0], Pattern.ParseDigits(""));
		}
	}
}
