﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VibroSleeve.Library.Scheduling;
using VibroSleeve.Library.Vibration;

namespace UnitTests
{
	[TestClass]
	public class TimeSlotTest
	{
    #region private

    private void AssertActiveMotors(TimeSlotCollection timeSlots, params int[] motors)
    {
      var actual = timeSlots.GetActiveMotors();
      CollectionAssert.AreEqual(motors, actual);
    }

    #endregion

    [TestMethod]
    public void ConvertPatternToTimeSlots()
    {
			var now = DateTime.Now;
	    var timeSlots = Pattern.Parse("3:12, 7:0, 20:5").AsTimeSlots(now);
			Assert.AreEqual(3, timeSlots.Length);

			// 10:12
			Assert.AreEqual(now, timeSlots[0].Begin);
			Assert.AreEqual(now.AddMilliseconds(3), timeSlots[0].End);
			CollectionAssert.AreEqual(new [] {1, 2}, timeSlots[0].Motors);

			// 10:0
			Assert.AreEqual(timeSlots[0].End.AddMilliseconds(1), timeSlots[1].Begin);
			Assert.AreEqual(now.AddMilliseconds(3 + 1 + 7), timeSlots[1].End);
			CollectionAssert.AreEqual(new int[0], timeSlots[1].Motors);

			// 10:5
			Assert.AreEqual(now.AddMilliseconds(3 + 1 + 7 + 1), timeSlots[2].Begin);
			Assert.AreEqual(timeSlots[2].Begin.AddMilliseconds(20), timeSlots[2].End);
			CollectionAssert.AreEqual(new[] { 5 }, timeSlots[2].Motors);
		}

    [TestMethod]
		public void SimpleTimeSlotTest()
		{
			var now = DateTime.Now;
			var timeSlots = new TimeSlotCollection();
			timeSlots.AddTimeSlots(new []
			{
				new TimeSlot(now, now.AddSeconds(1), 1, 2),
			});

		  timeSlots.UpdatePresent(DateTime.Now);

      AssertActiveMotors(timeSlots, 1, 2);
		}

		[TestMethod]
		public void TwoConcurrentMotors()
		{
			var now = DateTime.Now;
			var timeSlots = new TimeSlotCollection();
			timeSlots.AddTimeSlots(new[]
			{
				new TimeSlot(now, now.AddSeconds(1), 1),
				new TimeSlot(now, now.AddSeconds(1), 2),
			});

		  timeSlots.UpdatePresent(DateTime.Now);

		  AssertActiveMotors(timeSlots, 1, 2);
		}

    [TestMethod]
    public void Motor1_Overlapping_Motor2()
    {
      var now = DateTime.Now;
      var timeSlots = new TimeSlotCollection();
      timeSlots.AddTimeSlots(new[]
      {
        new TimeSlot(now, now.AddSeconds(1), 1),
        new TimeSlot(now.AddSeconds(1), now.AddSeconds(2), 2),
      });

      now = now.AddMilliseconds(500);
      timeSlots.UpdatePresent(now);
      AssertActiveMotors(timeSlots, 1);

      now = now.AddMilliseconds(500);
      timeSlots.UpdatePresent(now);
      timeSlots.RemovePast(now);
      AssertActiveMotors(timeSlots, 1, 2);

      now = now.AddMilliseconds(1);
      timeSlots.UpdatePresent(now);
      timeSlots.RemovePast(now);
      AssertActiveMotors(timeSlots, 2);
    }

    [TestMethod]
    public void Motor1_Pause_Motor2_Pause_Motor12()
    {
      var now = DateTime.Now;
      var timeSlots = new TimeSlotCollection();

      // timeSlots:                          [1]    [2]    [3]    [4]    [5]
      timeSlots.AddTimeSlots(Pattern.Parse("200:1, 200:0, 200:0, 200:0, 200:1").AsTimeSlots(now));
      timeSlots.AddTimeSlots(Pattern.Parse("200:0, 200:0, 200:2, 200:0, 200:2").AsTimeSlots(now));

      AssertActiveMotors(timeSlots, new int[0]);

      // [1]
      now = now.AddMilliseconds(10);
      timeSlots.UpdatePresentAndRemovePast(now);
      AssertActiveMotors(timeSlots, 1);

      // [2]
      now = now.AddMilliseconds(200);
      timeSlots.UpdatePresentAndRemovePast(now);
      AssertActiveMotors(timeSlots, new int[0]);

      // [3]
      now = now.AddMilliseconds(200);
      timeSlots.UpdatePresentAndRemovePast(now);
      AssertActiveMotors(timeSlots, 2);

      // [4]
      now = now.AddMilliseconds(200);
      timeSlots.UpdatePresentAndRemovePast(now);
      AssertActiveMotors(timeSlots, new int[0]);

      // [5]
      now = now.AddMilliseconds(200);
      timeSlots.UpdatePresentAndRemovePast(now);
      AssertActiveMotors(timeSlots, 1, 2);
    }

    [TestMethod]
    public void Motor12_Pause_Motor3_Pause_Motor12_Pause_Motor3()
    {
      var now = DateTime.Now;
      var timeSlots = new TimeSlotCollection();

      // [1]  [2]  [3]  [4]  [5]  [6]  [7]
      // 1,2  0    3    0    1,2  0    3

      timeSlots.AddTimeSlots(Pattern.Parse("10:1, 10:0, 15:0, 5:0, 10:12").AsTimeSlots(now));
      timeSlots.AddTimeSlots(Pattern.Parse("10:2, 10:0, 40:0, 10:3").AsTimeSlots(now));
      timeSlots.AddTimeSlots(Pattern.Parse("20:0, 10:3").AsTimeSlots(now));

      AssertActiveMotors(timeSlots, new int[0]);

      // [1]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(1));
      AssertActiveMotors(timeSlots, 1, 2);

      // [2]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(11));
      AssertActiveMotors(timeSlots, new int[0]);

      // [2]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(12));
      AssertActiveMotors(timeSlots, new int[0]);

      // [2]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(13));
      AssertActiveMotors(timeSlots, new int[0]);

      // [2]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(20));
      AssertActiveMotors(timeSlots, new int[0]);

      // [3]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(21));
      AssertActiveMotors(timeSlots, 3);

      // [4]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(32));
      AssertActiveMotors(timeSlots, new int[0]);

      // [5]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(44));
      AssertActiveMotors(timeSlots, 1, 2);

      // [6]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(55));
      AssertActiveMotors(timeSlots, new int[0]);

      // [7]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(66));
      AssertActiveMotors(timeSlots, 3);
    }

    [TestMethod]
    public void Motor123_Motor234_Motor5()
    {
      var now = DateTime.Now;
      var timeSlots = new TimeSlotCollection();

      //   [1]         [2]
      // 1,2,3,4    1,2,3,4,5

      timeSlots.AddTimeSlots(Pattern.Parse("10:123").AsTimeSlots(now));
      timeSlots.AddTimeSlots(Pattern.Parse("10:234, 10:1234").AsTimeSlots(now));
      timeSlots.AddTimeSlots(Pattern.Parse("10:0, 10:5").AsTimeSlots(now));

      AssertActiveMotors(timeSlots, new int[0]);

      // [1]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(1));
      AssertActiveMotors(timeSlots, 1, 2, 3, 4);

      // [2]
      timeSlots.UpdatePresentAndRemovePast(now.AddMilliseconds(11));
      AssertActiveMotors(timeSlots, 1, 2, 3, 4, 5);
    }
  }
}
