// DEMO MODE

int led = 3;

void setup() {
  // put your setup code here, to run once:
pinMode(led, OUTPUT);
}

void myDigital(int count, int high)
{
  int i;
  for (i = 0; i < count; i++)
  {
    digitalWrite(led, high);
  }
  digitalWrite(led, LOW);
}

void turnOn(int pin)
{
  analogWrite(pin, 200);
}

void turnOff(int pin)
{
  analogWrite(pin, 0);
}

void buzz(int amount, int duration, int* pins)
{
  for (int i = 0; i < amount; i++)
  {
    turnOn(pins[i]);
  }
  delay(duration);
  for (int i = 0; i < amount; i++)
  {
    turnOff(pins[i]);
  }
}

int randomPin()
{
  return 8 + random(5);
}

int pins_All[5] = { 8, 9, 10, 11, 12 };

void loop() {
  // put your main code here, to run repeatedly:
  

  for (int i = 8; i <= 12; i++)
  {
    int pins[1];
    pins[0] = i;
    buzz(1, 500, pins);
    delay(200);
  }

  //delay(500);
  buzz(5, 1000, pins_All);
  delay(1500);

  for(int i = 0; i < 40; i++)
  {
    if (random(2) == 0)
    {
      int pins[2] = { randomPin(), randomPin() };
      buzz(2, 100, pins);
    }
    else
    {
      int pins[1];
      pins[0] = randomPin();
      buzz(1, 100, pins);
    }
    delay(100);
  }

  delay(2000);
  
}
