// Pin <> Motor Assignment
const int pin_motor1 = 12;
const int pin_motor2 = 11;
const int pin_motor3 = 10;
const int pin_motor4 = 9;
const int pin_motor5 = 8;

const int pow_ON = HIGH;
const int pow_OFF = LOW;

// Bitmask Positions
const int bmpos_power = 0;
const int bmpos_motor1 = 1;
const int bmpos_motor2 = 2;
const int bmpos_motor3 = 3;
const int bmpos_motor4 = 4;
const int bmpos_motor5 = 5;
const int bmpos_unused1 = 6;
const int bmpos_unused2 = 7;

int pins_All[5] = { 8, 9, 10, 11, 12 };

void setup () {
  pinMode(pin_motor1, OUTPUT);
  pinMode(pin_motor2, OUTPUT);
  pinMode(pin_motor3, OUTPUT);
  pinMode(pin_motor4, OUTPUT);
  pinMode(pin_motor5, OUTPUT);
  Serial.begin(9600);
}

int input;

void loop() {
  int input = -1;
  if (Serial.available()) {
    input = Serial.read();
  }
  else {
    input = -1;
    return;
  }

  //input = input - '0';
  //Serial.println(input);

  bool power = bitRead(input, bmpos_power);
  bool motor1 = bitRead(input, bmpos_motor1);
  bool motor2 = bitRead(input, bmpos_motor2);
  bool motor3 = bitRead(input, bmpos_motor3);
  bool motor4 = bitRead(input, bmpos_motor4);
  bool motor5 = bitRead(input, bmpos_motor5);
  bool unused2 = bitRead(input, bmpos_unused2);
  bool unused1 = bitRead(input, bmpos_unused1);

  // DEMO MODE
  if (unused1) {
    demo_mode();
    return;
  }

  // switch on / off
  int pow_lvl = power ? pow_ON : pow_OFF;

  if (motor1)
    digitalWrite(pin_motor1, pow_lvl);

  if (motor2)
    digitalWrite(pin_motor2, pow_lvl);

  if (motor3)
    digitalWrite(pin_motor3, pow_lvl);

  if (motor4)
    digitalWrite(pin_motor4, pow_lvl);

  if (motor5)
    digitalWrite(pin_motor5, pow_lvl);
}

// DEMO SCRIPT
void demo_buzz(int amount, int duration, int* pins)
{
  for (int i = 0; i < amount; i++)
  {
    digitalWrite(pins[i], 200);
  }
  delay(duration);
  for (int i = 0; i < amount; i++)
  {
    digitalWrite(pins[i], 0);
  }
}

int randomPin()
{
  return 8 + random(5);
}

void demo_mode() {
    for (int i = 8; i <= 12; i++)
  {
    int pins[1];
    pins[0] = i;
    demo_buzz(1, 500, pins);
    delay(200);
  }

  //delay(500);
  int pins_All[5] = { 8, 9, 10, 11, 12 };
  demo_buzz(5, 1000, pins_All);
  delay(1500);

  for(int i = 0; i < 40; i++)
  {
    if (random(2) == 0)
    {
      int pins[2] = { randomPin(), randomPin() };
      demo_buzz(2, 100, pins);
    }
    else
    {
      int pins[1];
      pins[0] = randomPin();
      demo_buzz(1, 200, pins);
    }
    delay(100);
  }
  delay(1500);
}
