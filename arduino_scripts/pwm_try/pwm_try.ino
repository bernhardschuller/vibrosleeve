// DEMO MODE

int led = 3;

void setup() {
  // put your setup code here, to run once:
pinMode(led, OUTPUT);
}

void try_pwm() {
  for (int i = 0; i < 256; i++)
  {
    analogWrite(8, i);
    delay(20);
    analogWrite(8, 0);
    delay(5);
  }
}

void try_pwm2() {
  int i = 0;
  while (i < 50)
  {
    analogWrite(8, 200);
    delay(5);
    analogWrite(0, 200);
    delay(i);
    analogWrite(8, 200);
    delay(5);
    analogWrite(8, 0);

    delay (100);
    
    i = i + 5;
  }
}

void try_pwm3() {

  analogWrite(10, 200);
  delay(200);
  analogWrite(10, 0);
  
  int i = 20;
  while (i < 200)
  {
    analogWrite(8, 200);
    delay(i);
    analogWrite(8, 0);
    delay(500);

    i = i + 20;
  }
}

void pwm4_buzz(int pin, int duration) {
  analogWrite(pin, 200);
  delay(duration);
  analogWrite(pin, 0);
  delay(500);
}

void try_pwm4() {
  pwm4_buzz(8, 20);
  pwm4_buzz(8, 30);
  pwm4_buzz(8, 40);
  pwm4_buzz(8, 50);
  pwm4_buzz(8, 60);
  pwm4_buzz(8, 70);
  pwm4_buzz(8, 80);
}

void loop() {
    // demo_mode();
    try_pwm4();
    delay(3000);
}
